---
meta:
  description: Formulated on the accumulated knowledge of cannabis medicine and
    its potential therapeutic effects. Learn more about CANNACEUTICA and
    upcoming clinical studies.
  title: CANNACEUTICA | Cannabis With Evidence
  custommeta:
    - name: keywords
      content: keyword
template: CompanyPage
slug: company
companycontent: >-
  ## We Think big, to be Big


  We have already worked with projects that consist of many disciplines. We think Big to grow your Business.
companyBannerImage:
  image: /img/company-image.webp
  alt: company banner Image
codagecontainer:
  title: We aid our associates to grow their business
  content: Codage Habitation was started in 2020, by the motive of Grow and
    Escalate your Business. We have delivered our custom development projects
    for 24 countries, growing in number of employees.
companyservicescontainer:
  headercontent: >-
    ##### High-quality digital product is created on the basis of strategy,
    creativity and technology.


    The great software is developed only and only if it was communicated well when it was in the developing phase. Our Team is ready to help you because it’s the right thing to do.
  servicescontent:
    - title: Web Application Development
      description: Javascript, Headless CMS, Phython, PHP, Ecommerce
    - title: Mobile Application Development
      description: React native, Android, Flutter
    - title: Digital Marketing
      description: SEO, Link building, Content Marketing, Content Writing
globaldigitaltrends:
  content: >-
    ##### We not only follow trends, we generate them


    We assist businesses to become innovation leaders by providing them software programs on demand and according to their needs. We also assist you to select the ideal and appropriate technologies to invest in, choose the best and most effective design as well as procedures to follow and supervise the successful delivery of their software program.
companyworkcontainer:
  leftimage:
    image: /img/work-image1.webp
    alt: Work Image
  rightimage:
    image: /img/work-image2.webp
    alt: Work Image
  content: We have already assisted companies from Start-ups, NGOs and
    multinational companies that went public, and became one of the top
    companies in their field.
---
