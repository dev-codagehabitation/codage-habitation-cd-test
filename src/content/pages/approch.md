---
startsmall:
  heading: Start small
  title1: Development
  content1: We do Implementation in Sprints and Developing Potential Shippable products.
  title2: Testing
  content2: Analyse and Validating User Stories and  perform Regression tests and
    find raising defects.
  number1: "3"
  number2: "4"
meta:
  description: Formulated on the accumulated knowledge of cannabis medicine and
    its potential therapeutic effects. Learn more about CANNACEUTICA and
    upcoming clinical studies.
  title: CANNACEUTICA | Cannabis With Evidence
  custommeta:
    - name: keywords
      content: keyword
template: ApprochPage
slug: approch
approchontent: >-
  ## We Don’t Wait for Opportunity, We create it


  We create custom softwares that only helps your business to grow but also sets new trend in market.


  [](<>)
bannerButtons:
  - label: Plan creative brainstorm
    url: "#"
approchBannerImage:
  image: /img/approch-banner.webp
  alt: Approch Banner Image
thinkbig:
  heading: think big
  title1: Planning
  content1: In planning we first defining scopes in which we going to develop
    custom software after we establishing approach and define process and its
    standards.
  title2: Analysis
  content2: In this part first we take the requirement and then Analyse it prepare
    Architecture Design and Detailed UI / UX Design.
  number1: "1"
  number2: "2"
growsmart:
  heading: "Grow smart "
  title1: Deployment
  content1: Preparing a version for the client which client can use and gives its
    valuable review.
  title2: Evaluation
  content2: Proper Demo of a given version and evaluation.
  number1: "5"
  number2: "6"
---
