---
fullbannerimage:
  image: /img/home-image.webp
  alt: Full Banner
servicescontainer:
  headercontent: |-
    ###### Services

    ### We assist Business to become Innovation Leader
  servicescontent:
    - title: Web Application Development
      description: High-performance web applications that grow with your business
      serviceslist:
        - listitem: Javascript
        - listitem: Headless CMS
        - listitem: Phython
        - listitem: PHP
        - listitem: Ecommerce
      servicesbutton:
        url: /services
    - title: Mobile Application Development
      description: High-performance web applications that grow with your business
      serviceslist:
        - listitem: React native
        - listitem: Flutter
      servicesbutton:
        url: /services
    - title: Digital Marketing
      description: High-performance web applications that grow with your business
      serviceslist:
        - listitem: SEO
        - listitem: Link building
        - listitem: Content Marketing
        - listitem: Content Writing
      servicesbutton:
        url: /services
meta:
  description: Formulated on the accumulated knowledge of cannabis medicine and
    its potential therapeutic effects. Learn more about CANNACEUTICA and
    upcoming clinical studies.
  title: CANNACEUTICA | Cannabis With Evidence
  custommeta:
    - name: keywords
      content: keyword
template: HomePage
slug: ""
homecontent: |-
  # **Grow and Escalate** your Business with Us

  Because only Excellent Team can bring Excellence to your business.
homebutton:
  label: Lets discuss
  url: mailto:info@codagehabitation.com
workcontainer:
  headercontent: |-
    ###### Work

    ### Clients love to work with us
  workcontent:
    - title: CK Sports
      description: Web Application, Website and Mobile app
      worklink:
        url: "#"
      workimage:
        image: /img/ck-sports.png
        alt: ""
    - title: King 3 entertainment
      description: Web application and Digital Marketing
      worklink:
        url: "#"
      workimage:
        image: /img/king3event.png
testimonialscontainer:
  testimonialscontent: |-
    ###### Testimonials

    ### Why customers love working with us
  testimonials:
    - authorcontent: Codage Habitation is very responsive and truly trustworthy,
        Working with the codage habitation make my agency more efficient. Thanks
        to codage habitation’s services.
      authorname: "Gee "
      authorposition: Founder of King 3 Event - USA
      authorimage:
        image: /img/gee-client.png
        alt: Gee USA - Codage Habitation Client
content: ""
---
