---
developersQuotes:
  content: >-
    ### “Why our client recommend us?”


    Codage Habitation assists businesses to become innovation leaders by providing them software programs on demand and according to their needs. We also assist you to select the ideal and appropriate technologies to invest in, choose the best and most effective design as well as procedures to follow and supervise the successful delivery of their software program.
  developersquotesimage:
    image: /img/service-image.webp
    alt: Developers Quotes Image
meta:
  description: Formulated on the accumulated knowledge of cannabis medicine and
    its potential therapeutic effects. Learn more about CANNACEUTICA and
    upcoming clinical studies.
  title: CANNACEUTICA | Cannabis With Evidence
  custommeta:
    - name: keywords
      content: keyword
slug: services
mobileApplicationDevelopment:
  content: >-
    ## Mobile Application Development


    **Think a mobile app working accordingly the app you are fond of.** Our Team will assist you to finalize your design, create as well as release your software application that will certainly receive general recognition for its customer experience. Our Team has worked on multiple projects. Your app can be the user-friendly app that will keep your customes coming back. So, Our Software Development Company can be best for your Business.
  knowmorebutton: "#"
  webappbutton:
    - label: React native
      url: "#"
    - label: Flutter
      url: "#"
digitalmarketing:
  content: |-
    ## Digital marketing

    We have a best Digital Marketing team which can give\
    best recogniztion to your business to your niche audience.\
    Our team are the viral drivers of not just the likes,\
    comments and shares but also we are specialized in bringing in niche\
    customer, consistent growth, increased ranking, website tracking\
    and visits which can increase your customer Engagement.
  knowmorebutton: "#"
  webappbutton:
    - label: Seo
      url: "#"
    - label: Link building
      url: "#"
    - label: Content Marketing
      url: "#"
    - label: Content Writing
      url: "#"
template: ServicesPage
servicescontent: >-
  ###### WE ASSIST BUSINESS TO BECOME INNOVATION LEADER


  ## Custom Software Development Service which would take your business to Next Level


  Collaborate with product designers, frontend-backend developers, cloud architecture and DevOps. Complete team which can take your Business to the next level
ourservicesprovide:
  - content: >-
      ## Web Application Development


      We create B2B and B2C compatible web applications to meet your Business Challenges. Our Team develop best custom web application in 5 top demanding languages which help you to develop best custom web application, we have already developed web application for many top running business. **Great ideas comes by Experience by this motive our company lives by.**
    knowmorebutton: "#"
    applink:
      - label: Phython
        url: "#"
      - label: Headless CMS
        url: "#"
      - label: Javascript
        url: "#"
      - label: E-commerce
        url: "#"
      - label: PHP
        url: "#"
    ourservicesQuotes: []
  - content: >-
      ## Mobile Application Development


      **Think a mobile app working accordingly the app you are fond of.** Our Team will assist you to finalize your design, create as well as release your software application that will certainly receive general recognition for its customer experience. Your app can be the user-friendly app that will keep your customers coming back. One way or the various other software application development solutions will be best for your business.
    knowmorebutton: "#"
    applink:
      - label: React native
        url: "#"
      - label: Flutter
        url: "#"
    ourservicesQuotes: []
  - content: >-
      ## Digital marketing


      We have a best Digital Marketing team which can give best recogniztion to your business to your niche audience. Our team are the viral drivers of not just the likes, comments and shares but also we are specialized in bringing in niche customer, consistent growth, increased ranking, website tracking and visits which can increase your customer Engagement.
    knowmorebutton: "#"
    applink:
      - label: Seo
        url: "#"
      - label: Link building
        url: "#"
      - label: Content Marketing
        url: "#"
      - label: Content Writing
        url: "#"
    ourservicesQuotes: []
developersQuotes2:
  content: >-
    ### “Why our Client Recommend us?”


    Codage Habitation assists businesses to become innovation leaders by providing them software programs on demand and according to their needs. We also assist you to select the ideal and appropriate technologies to invest in, choose the best and most effective design as well as procedures to follow and supervise the successful delivery of their software program.
  developersquotesimage:
    image: /img/service-image.webp
    alt: Developers Quotes Image
WebApplicationDevelopment:
  content: >-
    ## Web Application Development


    We create B2B and B2C compatible web applications to meet your Business Challenges. Our Team develop best custom web application in 5 top demanding languages which help you to develop best custom web application, we have already developed web application for many top running business. **Great ideas comes by Experience by this motive our company lives by.**
  knowmorebutton: "#"
  webappbutton:
    - label: Phython
      url: "#"
    - label: Headless CMS
      url: "#"
    - label: Javascript
      url: "#"
    - label: E-commerce
      url: "#"
    - label: PHP
      url: "#"
---
