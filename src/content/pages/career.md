---
template: CareerPage
slug: "career"
meta:
  description: Formulated on the accumulated knowledge of cannabis medicine and
    its potential therapeutic effects. Learn more about CANNACEUTICA and
    upcoming clinical studies.
  title: CANNACEUTICA | Cannabis With Evidence  
  custommeta:
    - name: keywords
      content: keyword
---
