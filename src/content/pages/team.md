---
template: TeamPage
slug: team
meta:
  description: Formulated on the accumulated knowledge of cannabis medicine and
    its potential therapeutic effects. Learn more about CANNACEUTICA and
    upcoming clinical studies.
  title: CANNACEUTICA | Cannabis With Evidence
  custommeta:
    - name: keywords
      content: keyword
teamcontent: >-
  ## We have amazing people, who have a proactive attitude and technical attitude.


  They are the people who are not only following the tasks, but can work as a team, Together.
bannerButtons:
  - label: People
    url: "#"
  - label: Careers
    url: "#"
teamBannerImage:
  image: /img/team-banner.webp
  alt: Team Banner Image
teamtitle: Meet the team
meetTheTeam:
  - name: Urvish Prajapati
    position: Founder & Chief Executive Officer
    content: Tech nerd turned business bloke. Anthony started hacking code only to
      find that his obsession with tech was more about how he could use it to
      turn a buck for his clients. Started as a freelancer, now focused on
      bringing further growth to Hustle and its customers.
    notes: |-
      Bachelor of Information Technology (First Class Honours)
      Master of Commerce (Strategy & Marketing)
    teamMemberImage:
      image: /img/team-member1.webp
      alt: Team Member
  - name: Urvish Prajapati
    position: Founder & Chief Executive Officer
    content: Tech nerd turned business bloke. Anthony started hacking code only to
      find that his obsession with tech was more about how he could use it to
      turn a buck for his clients. Started as a freelancer, now focused on
      bringing further growth to Hustle and its customers.
    notes: |-
      Bachelor of Information Technology (First Class Honours)
      Master of Commerce (Strategy & Marketing)
    teamMemberImage:
      image: /img/team-member1.webp
      alt: Team Member
  - name: Urvish Prajapati
    position: Founder & Chief Executive Officer
    content: Tech nerd turned business bloke. Anthony started hacking code only to
      find that his obsession with tech was more about how he could use it to
      turn a buck for his clients. Started as a freelancer, now focused on
      bringing further growth to Hustle and its customers.
    notes: |-
      Bachelor of Information Technology (First Class Honours)
      Master of Commerce (Strategy & Marketing)
    teamMemberImage:
      image: /img/team-member1.webp
      alt: Team Member
  - name: Urvish Prajapati
    position: Founder & Chief Executive Officer
    content: Tech nerd turned business bloke. Anthony started hacking code only to
      find that his obsession with tech was more about how he could use it to
      turn a buck for his clients. Started as a freelancer, now focused on
      bringing further growth to Hustle and its customers.
    notes: |-
      Bachelor of Information Technology (First Class Honours)
      Master of Commerce (Strategy & Marketing)
    teamMemberImage:
      image: /img/team-member1.webp
      alt: Team Member
---
