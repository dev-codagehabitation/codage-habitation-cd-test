const workStrings =
	{
		title: "Work",
		subtitle: "What clients says about us",
		workTitle: "Lorem Ipsum",
		description: "1.4 mil website visitors per year",
		featuredCases: "Featured Cases",
		ourcases: 'Our cases',
		letsWork: "Let’s Work",
		startIntake: "Start intake",
		detailsTitle: "Lorem ipsum",
		detailsContent: "We are proud of our clients. Beautiful, driven brands with which we want to achieve the maximum every day. View our success stories here and get inspired.",
		detailsButtonslabel: 'Watch Website live',
		from_orientation_to_online_ordering: "From orientation to online ordering",
		from_orientation_to_online_ordering_description: "We are proud of our clients. Beautiful, driven brands with which we want to achieve the maximum every day. View our success stories here and get inspired.with which we want to achieve the maximum every day. View our success stories here and ge t inspired.with which we want to achieve the maximum every day. View our success stories here and ge t inspired.",
		simple_and_fast: "Simple and fast",
		continuous_testing_and_optimizing: "Continuous testing and optimizing",
		share_text: "Share the article with your friends:",
		read_stories: "Read other success stories"
	}

module.exports = workStrings