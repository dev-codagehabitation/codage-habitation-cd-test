import React, { Fragment } from 'react'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import Meta from '../Meta'
import Nav from '../Nav'
import Footer from '../Footer'
import { withPrefix } from 'gatsby'

import 'modern-normalize/modern-normalize.css'
import '../../styles/globalstyles.scss'

export default function Layout({ children, meta, title, isNavStyle, isDarkStyle }) {
  return (
    <StaticQuery
      query={graphql`
        query IndexLayoutQuery {
          settingsYaml {
            siteTitle
            siteDescription
            googleTrackingId
            socialMediaCard {
              image
            }
          }
          allPosts: allMarkdownRemark(
            filter: { fields: { contentType: { eq: "postCategories" } } }
            sort: { order: DESC, fields: [frontmatter___date] }
          ) {
            edges {
              node {
                fields {
                  slug
                }
                frontmatter {
                  title
                }
              }
            }
          }
        }
      `}
      
      render={data => {
        const { siteTitle, socialMediaCard, googleTrackingId } =
          data.settingsYaml || {},
          subNav = {
            posts: data.allPosts.hasOwnProperty('edges')
              ? data.allPosts.edges.map(post => {
                return { ...post.node.fields, ...post.node.frontmatter }
              })
              : false
          }

        return (
          <Fragment>
            <Helmet
              defaultTitle={siteTitle}
              titleTemplate={`%s | ${siteTitle}`}
            >
              <link rel="icon" href={`${withPrefix('/')}img/logo.svg`} />
              {title}
              {/* Add font link tags here */}
            </Helmet>

            <Meta
              googleTrackingId={googleTrackingId}
              absoluteImageUrl={
                socialMediaCard &&
                socialMediaCard.image &&
                socialMediaCard.image
              }
              {...meta}
              {...data.settingsYaml}
            />
            <div {...isDarkStyle}>
              {isDarkStyle ?
                <div className="darkOverlay"></div>
                : null
              }
            </div>
            <div {...isNavStyle} className={!isNavStyle ? "navDarkStyle" : "navLightStyle"}>
              <Nav subNav={subNav} />
            </div>
            <div className={!isNavStyle ? "darkContentStyle" : "lightContentStyle"}>
              {children}
            </div>
            <Footer />
          </Fragment>
        )
      }}
    />
  )
}
