import React, { useState, useEffect, useRef } from 'react'
import { Location } from '@reach/router'
import { Link } from 'gatsby'
import Logo from '../Logo'
import menuItems from '../../data/nav.json'
import { StaticImage } from "gatsby-plugin-image"
import { Strings } from "../../resources"
import './nav.scss'

const Navigation = (props) => {
  const [isSticky, setSticky] = useState(false);
  const [active, setAtive] = useState(false)

  const [currentPath, setCurrentPath] = useState('')


  const { location } = props

  const ref = useRef(null);
  const handleScroll = () => {
    if (ref.current) {
      setSticky(ref.current.getBoundingClientRect().top <= -100);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', () => handleScroll);
    };
  }, []);

  useEffect(() => {
    setCurrentPath(location.pathname)
  }, [location.pathname])

  const handleMenuToggle = () => setAtive(!active)

  // Only close nav if it is open
  const handleLinkClick = () => active && handleMenuToggle()
  // keyboard events
  const handleLinkKeyDown = ev => {
    if (ev.keyCode === 13) {
      active && handleMenuToggle()
    }
  }



  const navLinks = menuItems.topLevelItems.map((menuItem, index, to, className, children, ...props) => {
    const classes = (menuItem.itemType === "Link") ? "nav-item" : "btn btn-login";
    return (
      <div key={index} className={classes}>
        <Link
          to={menuItem.url}
          className={`navlink ${menuItem.url === currentPath ? 'active' : ''
            } `}
          onClick={handleLinkClick}
          onKeyDown={handleLinkKeyDown}
          tabIndex={0}
          aria-label="Navigation"
          role="button"
          {...props}
        >
          {menuItem.displayText}
        </Link>
      </div>
    );
  });

  const fullNavLinks = menuItems.fullNavItem.map((menuItem, index, to, className, children, ...props) => {
    const classes = (menuItem.itemType === "Link") ? "nav-item" : "nav-item";
    return (
      <div key={index} className={classes}>
        <Link
          to={menuItem.url}
          className={`navlink ${menuItem.url === currentPath ? 'active' : ''
            } `}
          onClick={handleLinkClick}
          onKeyDown={handleLinkKeyDown}
          tabIndex={0}
          aria-label="Navigation"
          role="button"
          {...props}
        >
          {menuItem.displayText}
        </Link>
      </div>
    );
  });

  const socialLinks = menuItems.socialNavItem.map((menuItem, index, to, className, children, ...props) => {
    const classes = (menuItem.itemType === "Link") ? "social-item" : "btn btn-login";
    return (
      <div key={index} className={classes}>
        <Link
          to={menuItem.url}
          className={`social-link ${menuItem.url === currentPath ? 'active' : ''
            } `}
          onClick={handleLinkClick}
          onKeyDown={handleLinkKeyDown}
          tabIndex={0}
          aria-label="Navigation"
          role="button"
          {...props}
        >
          {menuItem.displayText}
        </Link>
      </div>
    );
  });

  const navFooterItem = menuItems.fullNavFooterItem.map((menuItem, index, to, className, children, ...props) => {
    return (
      <div key={index}>
        <Link
          to={menuItem.url}
          className={`footer_links_item ${menuItem.url === currentPath ? 'active' : ''
            } `}
          onClick={handleLinkClick}
          onKeyDown={handleLinkKeyDown}
          tabIndex={0}
          aria-label="Navigation"
          role="button"
          {...props}
        >
          {menuItem.displayText}
        </Link>
      </div>
    );
  });

  if (typeof window !== 'undefined') {
    let prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
      const maxScroll = document.body.clientHeight - window.innerHeight;
      let currentScrollPos = window.pageYOffset;
      if (
        (maxScroll > 0 && prevScrollpos > currentScrollPos && prevScrollpos <= maxScroll)
        || (maxScroll <= 0 && prevScrollpos > currentScrollPos)
        || (prevScrollpos <= 0 && currentScrollPos <= 0)
      ) {
        document.getElementById("navbar").style.top = "0";
      } else {
        document.getElementById("navbar").style.top = "-7.0rem"; // adjustable based your need
      }
      prevScrollpos = currentScrollPos;
    }
  }

  return (
    <>
      <nav id="navbar" className={`nav ${active ? 'nav-active' : ''}`} ref={ref}>
        <div className="nav--section">
          <div className="nav--container container">
            <div className="logo-container">
              <Link
                to="/"
                onClick={handleLinkClick}
                onKeyDown={handleLinkKeyDown}
                tabIndex={0}
                aria-label="Navigation"
                role="button"
              >
                <Logo />
              </Link>
            </div>
            <div className="menu">
              {navLinks}
            </div>
            <button
              className="button-blank nav--menubutton"
              onClick={handleMenuToggle}
              tabIndex={0}
              aria-label="Navigation"
            >
              <span className="closeMenu-text">{Strings.close_menu}</span>
              {active ? <div className="menu-close" /> : <div className="menu-bar" />}
            </button>
          </div>
        </div>
      </nav>
      {
        active ?
          <div className="nav--fullpage">
            <div className="nav--image">
              <img src={menuItems.navimage.image} alt={menuItems.navimage.alt} />
            </div>
            <div className="fullNav--container">
              <div className="nav--menu">
                {fullNavLinks}
              </div>
              <div className="fullNav--footer">
                <div className="footer_links">
                  {navFooterItem}
                </div>
                <div className="social_links">
                  {socialLinks}
                </div>
              </div>
            </div>
          </div>
          : null
      }
    </>
  )
}

export default ({ subNav }) => (
  <Location>{route => <Navigation subNav={subNav} {...route} />}</Location>
)
