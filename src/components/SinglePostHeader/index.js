import React from 'react'
import PropTypes from 'prop-types'

import Content from '../Content'
import Image from '../Image'
import Button from '../Button'
import './postheader.scss'

export default function SinglePostHeader({
  title,
  date,
  subtitle,
  large,
  className = '',
  featuredImage,
  darkMode,
  bannerButtons
}) {

  if (large) className += ' pageheader-large'
  if (darkMode) className += ' pageheader-dark'
  return (
    <div className={`pageheader postheader ${className}`}>
      <div className="container">
        <div className="header-content">
          {date && (
            <time
              className="singlepost--meta--date"
              itemProp="dateCreated pubdate datePublished"
              date={date}
            >
              {date}
            </time>
          )}
          {title && (
            <h2 className="singlepost--title" itemProp="title">
              {title}
            </h2>
          )}
          <div className="post--author">
            <div className="post--authorimage">
            </div>
            <div className="post--authorcontent">
              <h6>Matthijs Piëst</h6>
              <p>COO at WieBetaaltWat</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
