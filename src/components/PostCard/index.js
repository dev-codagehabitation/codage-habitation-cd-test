import React, { Fragment } from 'react'
import { Link } from 'gatsby'

import Image from '../Image'
import './postcard.scss'
import { Strings } from '../../resources';

const PostCard = ({
  featuredimage,
  title,
  date,
  excerpt,
  slug,
  status,
  categories = [],
  className = '',
  ...props
}) => (
    <Fragment>
      {
        status === "Published" ?

          <div to={slug} className={`postcard ${className}`}>
            {featuredimage && (
              <div className="postCard--image relative">
                <Image imageInfo={featuredimage} alt={title} />
              </div>
            )}
            <div className="postcard--content">
              <div className="postcard--author">
                <div className="profile-image">
                  <Image imageInfo={featuredimage} alt={title} />
                </div>
                <div className="postcard--authorcontent">
                  <h6>Matthijs Piëst</h6>
                  <p>COO at WieBetaaltWat</p>
                </div>
                <div className="postcard-date">
                  January 11th, 2022
                </div>
              </div>
              {title && <h3 className="postcard--title">{title}</h3>}
            </div>
            <Link
              to={slug}
              className="explore-link"
            >
              {Strings.Explore_Services}
            </Link>
          </div>
          : null

      }
    </Fragment>
  )

export default PostCard
