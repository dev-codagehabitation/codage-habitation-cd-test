import React from 'react'
import PropTypes from 'prop-types'

import Content from '../Content'
import Image from '../Image'
import Button from '../Button'
import './pageheader.scss'

export default function PageHeader({
  bannercontent,
  homeBannerImage,
  homebutton,
  title,
  subtitle,
  large,
  className = '',
  featuredImage,
  darkMode,
  bannerButtons
}) {

  if (large) className += ' pageheader-large'
  if (darkMode) className += ' pageheader-dark'
  return (
    <div className={`pageheader relative ${className}`}>
      <div className="container relative">
        <div className="header-content">
          <Content source={bannercontent} />
          {homebutton &&
            <Button
              label={homebutton.label}
              link={homebutton.url}
              icon
            />
          }
          {!!bannerButtons &&
            <div className="buttons-group">
              {bannerButtons.map((item, index) => (
                <div className="ButtonItem" key={index}>
                  <Button
                    whiteStyle
                    to={item.url}
                    label={item.label}
                    icon
                  />
                </div>
              )
              )}
            </div>
          }
        </div>
      </div>
      {homeBannerImage && (
        <section className="banner-image">
          <div className="desktopShow">
            <Image imageInfo={homeBannerImage} />
          </div>
        </section>
      )}
    </div>
  )
}
