import React, { useState, useEffect } from 'react'

import PostCard from '../PostCard'
import './postsection.scss'

const defaultProps = {
  posts: [],
  title: '',
  limit: 12,
  showLoadMore: true,
  loadMoreTitle: 'Load More',
  perPageLimit: 12,
  date: '',
}

const PostSection = (props = defaultProps) => {
  const [limit, setLimit] = useState(12)
  const { posts, title, showLoadMore, loadMoreTitle, perPageLimit, date } = props
  const visiblePosts = posts.slice(0, limit || posts.length)
  useEffect(() => {
    setLimit(props.limit)
  }, [props.limit])


  const increaseLimit = () => setLimit(limit + perPageLimit)

  return (
    <div className="postsection">
      {title && <h2 className="postsection--title">{title}</h2>}
      {!!visiblePosts.length && (
        <div className="postsection--grid">
          {visiblePosts.map((post, index) => (
            <PostCard key={post.title + index} {...post} />
          ))}
        </div>
      )
      }
      {
        showLoadMore && visiblePosts.length < posts.length && (
          <div className="taCenter">
            <button className="button" onClick={increaseLimit}>
              {loadMoreTitle}
            </button>
          </div>
        )
      }
    </div >
  )
}

export default PostSection
