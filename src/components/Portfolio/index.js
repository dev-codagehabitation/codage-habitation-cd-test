import React from 'react'
import PropTypes from 'prop-types'
import Button from '../Button'
import './portfolioStyle.scss'
import { StaticImage } from "gatsby-plugin-image"
import { Strings } from '../../resources';
import workStrings from '../../data/work'

export default function Portfolio({
  title,
  subtitle,
  className,
  items,
  ButtonLabel,
  ButtonLink,
  loadMore
}) {

  return (
    <div className={`${className}`}>
      <div className="container">
        <div className="section-header">
          <h2>{title}</h2>
          {ButtonLabel &&
            <Button
              blackStyle
              label={ButtonLabel}
              link={ButtonLink}
              icon
            />
          }
        </div>
        <div className="portfolio-grid">
          {!!items &&
            items.map((item, index) => (
              <div className="portfolio-item" key={index}>
                <div className="work-image">
                  <StaticImage
                    src="../../img/image.webp"
                    quality={95}
                    formats={["auto", "webp", "avif"]}
                    alt="Grow and Escalate"
                  />
                </div>
                <div className="portfolio-content">
                  <h3>{workStrings.workTitle}</h3>
                  <p>{workStrings.description}</p>
                </div>
              </div>
            )
            )}
        </div>
        <div className="loadMore-footer">
          {loadMore &&
            <Button
              blackStyle
              label={Strings.LOADMORE}
              icon
            />
          }
        </div>
      </div>
    </div>
  )
}
