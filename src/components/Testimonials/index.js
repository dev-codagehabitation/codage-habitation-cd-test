import React from 'react'
import './testimonials.scss'
import { StaticImage } from "gatsby-plugin-image"
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Slider from "react-slick";
import Image from '../Image';
import Content from '../Content';

export default function Services({ items }) {
  const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    speed: 500,
    autoplaySpeed: 1500,
    passive: false
  };
  return (
    <section className="section Testimonials">
      <div className="container">
        <div className="top-content taCenter">
          <Content source={items.testimonialscontent} />
        </div>
        <div className="testimonial-tabContainer isShowDesktop">
          <Tabs>
            {!!items.testimonials &&
              items.testimonials.map((item, index) => (
                <TabPanel key={index}>
                  <div className="testimonial-content">
                    <p>{item.authorcontent}</p>
                  </div>
                </TabPanel>
              )
              )
            }
            <TabList>
              {!!items.testimonials &&
                items.testimonials.map((item, index) => (
                  <Tab key={index}>
                    <div className="testimonial-header">
                      <div className="image">
                        <Image imageInfo={item.authorimage} />
                      </div>
                      <div className="right">
                        <h6>{item.authorname}</h6>
                        <p>{item.authorposition}</p>
                      </div>
                    </div>
                  </Tab>
                )
                )}
            </TabList>
          </Tabs>
        </div>
        <div className="swiper-container testimonial-swiper isShowMobile">
          <Slider {...settings} className="swiper" currentSlide={0}>
            {!!items.testimonials &&
              items.testimonials.map((item, index) => (
                <div className="testimonial-item" key={index}>
                  <div className="testimonial-content">
                    <p>{item.authorcontent}</p>
                  </div>
                  <div className="testimonial-header">
                    <div className="image">
                      <Image imageInfo={item.authorimage} />
                    </div>
                    <div className="right">
                      <h6>{item.authorname}</h6>
                      <p>{item.authorposition}</p>
                    </div>
                  </div>
                </div>
              )
              )}
          </Slider>
        </div>

      </div>
    </section>
  );
}