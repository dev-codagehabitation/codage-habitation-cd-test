import React from 'react'
import './blogs.scss'
import { StaticImage } from "gatsby-plugin-image"
import { Link } from 'gatsby'
import Button from '../Button'
import Slider from "react-slick";
import { Strings } from '../../resources';

export default function Blogs(props) {

  const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    speed: 500,
    autoplaySpeed: 1500,
  };

  return (
    <section className="section Blogs">
      <div className="container">
        <div className="top-content taCenter">
          <h6 className="opacity">{Strings.blogs}</h6>
          <h3>{Strings.what_clients_says}</h3>
        </div>
        <div className="blogs_container isShowDesktop">
          {!!props.items &&
            props.items.map((item, index) => (
              <div className="blog-item" key={index}>
                <div className="blog-image">
                  <StaticImage
                    src="../../img/image.webp"
                    quality={95}
                    formats={["auto", "webp", "avif"]}
                    alt="Grow and Escalate"
                  />
                </div>
                <div className="blog-content">
                  <div className="profile-header">
                    <StaticImage
                      src="../../img/image.webp"
                      quality={95}
                      formats={["auto", "webp", "avif"]}
                      alt=""
                      className="profile-image"
                    />
                    <div className="profile-content">
                      <h6>Matthijs Piëst</h6>
                      <p>COO at WieBetaaltWat</p>
                    </div>
                    <div className="profile-date">
                      December 1,2021
                  </div>
                  </div>
                  <h5>Do I really need a skilled Software Architect? Guide for CTOs </h5>
                </div>
                <Link
                  to="/"
                  className="explore-link"
                >
                  Explore This Services
              </Link>
              </div>
            )
            )}

        </div>
        <div className="blogs_container isShowMobile">
          <Slider {...settings} className="swiper" currentSlide={0}>
            {!!props.items &&
              props.items.map((item, index) => (
                <div className="blog-item" key={index}>
                  <div className="blog-image">
                    <StaticImage
                      src="../../img/image.webp"
                      quality={95}
                      formats={["auto", "webp", "avif"]}
                      alt="Grow and Escalate"
                    />
                  </div>
                  <div className="blog-content">
                    <div className="profile-header">
                      <StaticImage
                        src="../../img/image.webp"
                        quality={95}
                        formats={["auto", "webp", "avif"]}
                        alt=""
                        className="profile-image"
                      />
                      <div className="profile-content">
                        <h6>Matthijs Piëst</h6>
                        <p>COO at WieBetaaltWat</p>
                      </div>
                      <div className="profile-date">
                        December 1,2021
                  </div>
                    </div>
                    <h5>Do I really need a skilled Software Architect? Guide for CTOs </h5>
                  </div>
                  <Link
                    to="/"
                    className="explore-link"
                  >
                    Explore This Services
              </Link>
                </div>
              )
              )}
          </Slider>
        </div>
        <div className="bottom-content taCenter">
          <Button
            label={Strings.see_all}
            icon
          />
        </div>
      </div>
    </section>
  );
}