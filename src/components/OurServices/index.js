import React from 'react'
import './ourServices.scss'
import { StaticImage } from "gatsby-plugin-image"
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Slider from "react-slick";
import Content from '../Content';
import Button from '../Button';
import { Strings } from '../../resources';
import Image from '../Image';

export default function OurServices(props) {

  return (
    <section className="section-mainContainer">
      {!!props.items &&
        props.items.map((item, index) => (
          <div className="section services-rowSection" key={index}>
            <div className="container">
              <div className="services-row">
                <div className="services-content">
                  <div className="numberStyle">{`0` + (index + 1)}</div>
                  <Content source={item.content} />
                  {/* <Button
                    link={item.knowmorebutton}
                    label={Strings.KNOW_MORE}
                    icon
                  /> */}
                </div>
                <div className="services-right">
                  <div className="services-provide">
                    {!!item.applink &&
                      item.applink.map((item, index) => (
                        <div className="button-item" key={index}>
                          <Button
                            outline
                            to={item.url}
                            label={item.label}
                          />
                        </div>
                      )
                      )
                    }
                  </div>
                </div>
              </div>
              {/* {item.ourservicesQuotes.map((item, index) => (
                <div className="services-row services-row-withImage">
                  <div className="services-image">
                    <Image imageInfo={item.quotesimage} />
                  </div>
                  <div className="services-content">
                    <Content source={item.quotescontent} />
                  </div>
                </div>
              ))
              } */}
            </div>
          </div>
        )
        )
      }
    </section>
  );
}