import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
import workStrings from '../../data/work'

import './stories.scss'
export default function Stories({ items, className }) {
  return (
    <div className={`work_container ${className}`}>
      {!!items &&
        items.map((item, index) => (
          <div className="work-item" key={index}>
            <div className="work-image">
              <StaticImage
                src="../../img/image.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
            </div>
            <div className="work-content">
              <h3>{workStrings.workTitle}</h3>
              <p>{workStrings.description}</p>
            </div>
          </div>
        )
        )}
    </div>
  )
}
