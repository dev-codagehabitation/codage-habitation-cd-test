import React from 'react'
import './spinner.scss'

export default () => (
  <div className='spinner'>
    <div className='semipolar-spinner'>
      <div className='ring' />
      <div className='ring' />
      <div className='ring' />
      <div className='ring' />
      <div className='ring' />
    </div>
  </div>
)
