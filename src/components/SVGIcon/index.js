import React from 'react'

import './svgicon.scss'

export default ({ src }) => {
  const icon = {
    maskImage: `url(${src})`,
    WebkitMaskImage: `url(${src})`
  }
  return (
    <div className="svgicon">
      <div className="svgicon--icon" style={icon} />
    </div>
  )
}
