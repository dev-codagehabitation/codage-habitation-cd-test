import React from 'react'
import './logo.scss'
import headerData from '../../data/header.json'
import { StaticImage } from "gatsby-plugin-image"

export default function Logo() {
  return (
    <div className="logo">
      <StaticImage
        src={"../../img/logo.svg"}
        quality={95}
        formats={["auto", "webp", "avif"]}
        alt={headerData.headerlogo.alt}
        className="logo-black"
      />
      <StaticImage
        src={"../../img/logo-white.svg"}
        quality={95}
        formats={["auto", "webp", "avif"]}
        alt={headerData.headerlogo.alt}
        className="logo-white"
      />
    </div>
  )
}