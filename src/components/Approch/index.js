import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
import workStrings from '../../data/work'

import './approch.scss'
export default function Approch({
  className,
  items
}) {
  return (
    <div className={`approch-row ${className}`}>
      <div className="approch-content">
        <div className="approch-heading">{items.heading}</div>
      </div>
      <div className="approch-content">
        <div className="approch-content-row">
          <h3><span>{items.number1} </span>{items.title1}</h3>
          <p>{items.content1}</p>
        </div>
        <div className="approch-content-row">
          <h3><span>{items.number2} </span>{items.title2}</h3>
          <p>{items.content2}</p>
        </div>
      </div>
    </div>
  )
}
