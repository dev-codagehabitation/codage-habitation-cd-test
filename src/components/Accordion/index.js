import React from 'react'
import './accordion.scss'
import Content from '../Content'
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from 'react-accessible-accordion';

export default function AccordionPage(props) {
  return (


    <Accordion allowZeroExpanded>
      {!!props.items &&
        props.items.map((item, index) => (
          <AccordionItem activeClassName="accordion__item isAccordion" key={index}>
            <AccordionItemHeading>
              <AccordionItemButton>
                {item.accordionHeading}
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <Content src={item.accordionPanel} />
            </AccordionItemPanel>
          </AccordionItem>
        )
        )}
    </Accordion>

  );
}