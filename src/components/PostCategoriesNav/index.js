import React from 'react'
import { Link } from 'gatsby'

import BlogSearch from '../BlogSearch'
import './postcategoriesnav.scss'
import {Strings} from '../../resources'

const PostCategoriesNav = ({ categories, enableSearch }) => (
  <div className="postcategories-navcontainer">
    <div className="postcategoriesnav">
      <Link className="navlink" exact="true" to={`/blogs/`}>
        {Strings.all_category_text} 
      </Link>
      {categories.map((category, index) => (
        <Link
          exact="true"
          className="navlink"
          key={category.title + index}
          to={category.slug}
        >
          {category.title}
        </Link>
      ))}
    </div>
    {enableSearch && <BlogSearch />}
   </div>
)

export default PostCategoriesNav
