import React from 'react'
import Content from '../Content'
import Button from '../Button'
import Consultation from '../../data/consultation.json'

import './bookConsultation.scss'

export default function BookConsultation(props) {
  return (
    <section className="reviews-section">
      <div className="container">
        <div className="reviews-online dark">
          <div className="left">
            <Content source={Consultation.reviewsonline.content} />
          </div>
          <Button
            link={Consultation.reviewsonline.bookfreeconsultation.url}
            label={Consultation.reviewsonline.bookfreeconsultation.label}
            icon
          />
        </div>
      </div>
    </section>
  );
}