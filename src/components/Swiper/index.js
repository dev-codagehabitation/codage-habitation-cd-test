import React from 'react'
import PropTypes from 'prop-types'

import Content from '../Content'

import Slider from "react-slick";

import './swiper.scss'

const Swiper = (props, {
  mode,
  className = '',
}) => {

  const settings = {
    dots: true,
    arrows: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    speed: 500,
    autoplaySpeed: 1500,
    // cssEase: "linear"
    // swipeToSlide: true,
  };

  if (props.mode) className += 'swiper'
  return (


    <Slider {...settings} className={`${className}`} currentSlide={0}>
      {!!props.items &&
        props.items.map((item, index) => (
          <div className="fourkeycannabinoids" key={index}>
            <Content source={item.fourkeycannabinoidscontent} />
          </div>
        )
        )}
    </Slider>


  )
}

Swiper.propTypes = {
  swipercontainer: PropTypes.shape({
    swipertabname: PropTypes.string,
    swipernotes: PropTypes.string,
  }),
}

export default Swiper
