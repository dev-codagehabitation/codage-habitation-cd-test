import React from 'react'
import './footer.scss'
import footerData from '../../data/footer.json'
import Button from '../Button'
import { Link } from 'gatsby'

export default function Footer() {

  const footerNavigation = footerData.footernav.map((menuItem, index, to, className, children, ...props) => {

    return (
      <div key={index} className="footer-navItem">
        <Link
          to={menuItem.url}
          className="footer-navLink"
          tabIndex={0}
          aria-label="Navigation"
          role="button"
          {...props}
        >
          {menuItem.displayText}
        </Link>
      </div>
    );
  });

  const footerSocialLinks = footerData.footerSocial.map((menuItem, index, to, className, children, ...props) => {

    return (
      <div key={index} className="footer-navItem">
        <Link
          to={menuItem.url}
          className="footer-navLink"
          tabIndex={0}
          aria-label="Navigation"
          role="button"
          {...props}
        >
          {menuItem.displayText}
        </Link>
      </div>
    );
  });

  const footerLinks = footerData.footerlinks.map((footerLinkItem, index, to, className, children, ...props) => {

    return (
      <div key={index} className="footer-navItem">
        <Link
          to={footerLinkItem.url}
          className="footer-navLink"
          tabIndex={0}
          aria-label="Navigation"
          role="button"
          {...props}
        >
          {footerLinkItem.displayText}
        </Link>
      </div>
    );
  });

  const footer_buttons = footerData.contactinformation.footerButtons.map((footerButtonItem, index, to, className, children, ...props) => {

    return (
      <div key={index} className="footerButtonItem">
        <Button
          link={footerButtonItem.url}
          label={footerButtonItem.displayText}         
          icon
        />
      </div>

    );
  });

  return (
    <footer className="footer">
      <div className="footer-main">
        <div className="container">
          <div className="footer-content">
            <div className="footer-title">{footerData.title}</div>
          </div>
          <div className="footer-top">
            <div className="footer-nav">
              <div className="footer-nav-left">
                {footerNavigation}
              </div>
              <div className="footer-nav-right">
                {footerSocialLinks}
              </div>
            </div>
            <div className="footer-right">
              <a
                href={`mailto:` + footerData.contactinformation.email}
                className="footer-link"
              >
                {footerData.contactinformation.email}
              </a>
              <div className="buttons-group">
                {footer_buttons}
              </div>
            </div>
          </div>
          <div className="footer-bottom">
            <div className="copyright">{footerData.copyrightText}</div>
            <div className="footerLinks">
              {footerLinks}
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}