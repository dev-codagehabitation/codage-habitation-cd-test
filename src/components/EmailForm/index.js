import React from 'react'
import { navigate } from 'gatsby'
import { Location } from '@reach/router'
import qs from 'qs'
import { Strings } from '../../resources'
import Button from '../../components/Button'
import './emailform.scss'

export default ({ pageCount }) => {
  return (
    <Location>
      {({ location }) => {
        return (
          <div className="email-form">
            <input
              type="text"
              placeholder={Strings.email_form_text}
              className="email-input"
            />
            <Button
              label={Strings.lets_learn}
              icon
            />
          </div>
        )
      }}
    </Location>
  )
}
