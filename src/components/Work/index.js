import React from 'react'
import './work.scss'
import { StaticImage } from "gatsby-plugin-image"
import Slider from "react-slick";
import workStrings from '../../data/work'
import Image from '../Image'

export default function Work({
  items,
  className = ''
}) {

  const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    speed: 500,
    autoplaySpeed: 1500,
  };

  return (
    <section className="section work">
      <div className="container">
        <div className="top-content taCenter">
          <h6 className="opacity">{workStrings.title}</h6>
          <h3>{workStrings.subtitle}</h3>
        </div>
        <div className="work_container isShowDesktop">
          {!!items &&
            items.map((item, index) => (
              <div className="work-item" key={index}>
                <div className="work-image">
                  <Image imageInfo={item.workimage} />                  
                </div>
                <div className="work-content">
                  <h3>{item.title}</h3>
                  <p>{item.description}</p>
                </div>
              </div>
            )
            )}
        </div>
        <div className="work_container isShowMobile">
          <Slider {...settings} className="swiper" currentSlide={0}>
            {!!items &&
              items.map((item, index) => (
                <div className="work-item" key={index}>
                  <div className="work-image">
                    <Image imageInfo={item.workimage} />                  
                  </div>
                  <div className="work-content">
                    <h3>{item.title}</h3>
                    <p>{item.description}</p>
                  </div>
                </div>
              )
              )}
          </Slider>
        </div>
      </div>
    </section>
  );
}