import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 25px;
  height: 38px;
  user-select: none;
  transform: translate(-50%, -50%);
  background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='30' height='40' viewBox='0 0 30 40'%3E%3Cdefs%3E%3Cstyle%3E.a%7Bfill:%23ffa400;%7D%3C/style%3E%3C/defs%3E%3Cpath class='a' d='M13.458,39.193C2.107,22.737,0,21.048,0,15a15,15,0,0,1,30,0c0,6.048-2.107,7.737-13.458,24.193a1.876,1.876,0,0,1-3.083,0ZM15,21.25A6.25,6.25,0,1,0,8.75,15,6.25,6.25,0,0,0,15,21.25Z'/%3E%3C/svg%3E");
  background-size:100% 100%;
  cursor: ${(props) => (props.onClick ? 'pointer' : 'default')};
  &:hover {
    z-index: 1;
  }
`;

const Marker = ({ text, onClick }) => (
  <Wrapper
    alt={text}
    onClick={onClick}
  />
);

Marker.defaultProps = {
  onClick: null,
};

Marker.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
};

export default Marker;