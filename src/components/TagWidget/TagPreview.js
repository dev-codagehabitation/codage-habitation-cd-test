import React, {Component} from 'react';

import "./tagstyles.scss"

export class TagPreview extends Component {
  render() {
    return React.createElement('ul', {
      className: "tags",
    },
      this.props.value.map(function(value, index) {
        return React.createElement('li', {
          className: "tag",
          key: index
        }, value)
      })
    )
  };
}
export default TagPreview