import React from 'react'
import { Link } from 'gatsby'
import { Strings } from '../../resources';

import './services.scss'

export default function Services({
  items,
  className = ''
}) {
  return (
    <div className={`services-grid ${className}`}>
      {!!items &&
        items.map((item, index) => (
          <div className="services-item" key={index}>
            <div className="services-header">
              <span>0{index + 1}</span>
              <h3>{item.title}</h3>
            </div>
            <div className="services-content">
              <p>{item.description}</p>
              {item.serviceslist &&
                <ul>
                  {item.serviceslist.map((item, index) => (
                    <li key={index}>
                      {item.listitem}
                    </li>
                  ))}
                </ul>
              }
            </div>
            {item.servicesbutton && (
              <Link
                to={item.servicesbutton.url}
                className="explore-link"
              >
                {Strings.Explore_Services}
              </Link>
            )}
          </div>
        )
        )
      }
      
    </div>
  )
}
