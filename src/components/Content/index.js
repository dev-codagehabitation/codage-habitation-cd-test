import React, { FunctionComponent, HTMLAttributes } from 'react';
import ReactMarkdown from "react-markdown";
import PropTypes from 'prop-types'
import Image from '../Image'

import './content.scss'

const encodeMarkdownURIs = (source = '') => {
  const markdownLinkRegex = /\[(.+)\]\((.+)(".+)\)/g
  // console.log(source)
  return source.replace(markdownLinkRegex, (match, linkURI) => {
    if (!linkURI) return match
    const replaced = match.replace(linkURI, encodeURI(linkURI))
    return replaced
  })
}

const MyImage = ({ nodeKey, src, title, alt }) => {

  return (
    <div>
      <Image
        className="content--image markdown-preview"
        imageInfo={{ 'image': src, alt }}
      />
    </div>
  )
}

const HtmlBlock = ({ value }) => {
  if (value.indexOf('<iframe') !== 0) return value
  return (
    <div
      className={`content--iframe`}
      dangerouslySetInnerHTML={{
        __html: value
      }}
    />
  )
}
export default function Content({
  source,
  src,
  className = ''
}) {

  // accepts either html or markdown
  source = source || src || ''
  if (source.match(/^</)) {

    return (
      <div
        className={`content ${className}`}
        dangerouslySetInnerHTML={{ __html: source }}
      />
    )
  }

  return (
    <ReactMarkdown
      className={`content ${className}`}
    >
      {source}
    </ReactMarkdown>
  )
}

Content.propTypes = {
  source: PropTypes.string,
  src: PropTypes.string,
  className: PropTypes.string
}

