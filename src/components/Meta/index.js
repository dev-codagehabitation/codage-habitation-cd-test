import React from 'react'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import { setPageTitle } from "../../utils/page";

export const query = graphql`
  fragment Meta on MarkdownRemark {
    frontmatter {
      meta {
        title
        description
        noindex
        canonicalLink
        custommeta{
          name
          content
        }
      }
    }
  }
`

const Meta = (props) => {
  const {
    title,
    url,
    description,
    absoluteImageUrl = '',
    twitterSiteAccount,
    twitterCreatorAccount,
    noindex,
    canonicalLink,
    siteTitle,
    siteDescription,
    googleTrackingId,
    custommeta
    // overwrite { title, description } if in fields or fields.meta
  } = props

  const imageUrl = absoluteImageUrl ? absoluteImageUrl : ''
  return (
    <Helmet>
      {title && <title>{setPageTitle(title)}</title>}
      {title && <meta property="og:title" content={setPageTitle(title)} />}
      {description && <meta name="description" content={description} />}
      {description && (
        <meta property="og:description" content={description} />
      )}
      {url && <meta property="og:type" content="website" />}
      {url && <meta property="og:url" content={url} />}
      {twitterSiteAccount && (
        <meta name="twitter:site" content={twitterSiteAccount} />
      )}
      {twitterCreatorAccount && (
        <meta name="twitter:creator" content={twitterCreatorAccount} />
      )}
      {noindex && <meta name="robots" content="noindex" />}
      {canonicalLink && <link rel="canonical" href={canonicalLink} />}

      {custommeta &&
        custommeta.map((custommeta, index) => (
          <meta name={custommeta.name} content={custommeta.content} key={index} />
        ))
      }
      <meta property="og:locale" content="en_US" />
      <meta property="og:site_name" content={siteTitle} />
      <meta name="twitter:description" content={siteDescription} />
      <meta name="twitter:title" content={siteTitle} />
      <meta name="twitter:image" content={imageUrl} />
      <meta property="og:image:secure_url" content={imageUrl} />
      <meta property="og:image" content={imageUrl} />
      <meta name="twitter:card" content={imageUrl} />

      {googleTrackingId && (
        <script
          async
          src={`https://www.googletagmanager.com/gtag/js?id=${googleTrackingId}`}
        />
      )}

      {googleTrackingId && (
        <script>
          {`
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '${googleTrackingId}');
              `}
        </script>
      )}
    </Helmet>
  )
}
export default Meta