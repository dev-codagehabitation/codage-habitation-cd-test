import React from 'react'
import { Link } from 'gatsby'

import './navlink.scss'

export default function NavLink({ className, children, ...props }) {
  return (
    <Link {...props} className={`navlink ${className || ''}`}>
      {children}
    </Link>
  )
}
