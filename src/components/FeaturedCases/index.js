import React from 'react'
import PropTypes from 'prop-types'
import Button from '../Button'
import Stories from '../Stories'
import './featuredCases.scss'

const storiesArray = [1, 2, 3]
export default function FeaturedCases({ title, items, className }) {
  return (
    <div className={`featured-cases-container ${className}`}>
      <div className="container">
        <div className="featured-cases-content">
          <h5>{title}</h5>
        </div>
        <Stories items={storiesArray} />
      </div>
    </div>
  )
}
