import React from 'react'
import PropTypes from 'prop-types'
import './button.scss'

export default function Button({
  outline,
  link,
  label,
  onClick,
  target,
  className = '',
  icon = '',
  blackStyle = '',
  whiteStyle = ''
}) {

  if (outline) className += ' button-outline'
  if (icon) icon += ' btnIcon'
  if (blackStyle) blackStyle += ' blackStyle'
  if (whiteStyle) whiteStyle += ' whiteStyle'

  return (
    <a href={link} onClick={onClick} className={`button ${className} ${icon} ${blackStyle} ${whiteStyle}`} target={target}>
      {label}
    </a>
  )
}
