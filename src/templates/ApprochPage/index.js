import React from 'react'
import Layout from '../../components/Layout'
import Image from '../../components/Image';
import PageHeader from '../../components/PageHeader'
import BookConsultation from '../../components/BookConsultation'
import Approch from '../../components/Approch'

import { graphql } from 'gatsby'

import './approch.scss'

export const ApprochPageTemplate = ({ approchontent, bannerButtons, approchBannerImage, thinkbig, startsmall, growsmart }) => {
  return (
    <main className="Approch">
      <PageHeader
        darkMode
        bannercontent={approchontent}
        //bannerButtons={bannerButtons}
        className="innerpage--content team--headercontent"
      />
      <div className="banner-container">
        <div className="container">
          <div className="banner-image">
            <Image imageInfo={approchBannerImage} />
          </div>
        </div>
      </div>
      <section className="section approch-container">
        <div className="container">
          <Approch
            items={thinkbig}
            className="approch-shape-after"
          />
          <Approch
            items={startsmall}
          />
          <Approch
            items={growsmart}
            className="approch-shape-before"
          />
        </div>
      </section>
      <BookConsultation />
    </main>
  )
}


const ApprochPage = ({ data: { page } }) => (
  <Layout isNavStyle meta={page.frontmatter.meta || false}>
    <ApprochPageTemplate {...page} {...page.frontmatter} body={page.html} />
  </Layout>
)
export default ApprochPage

export const pageQuery = graphql`
  ## Query for ApprochPage data
  ## Use GraphiQL interface (http://localhost:8000/___graphql)
  ## $id is processed via gatsby-node.js
  ## query name must be unique to this file
  query ApprochPage($id: String!) {
    page: markdownRemark(id: { eq: $id }) {
      ...Meta
      html
      frontmatter {
        approchontent
        bannerButtons{
          label
          url
        }
        approchBannerImage{
          image {
            childImageSharp {
              fluid(maxWidth: 800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }   
          alt
        }
        thinkbig{
          heading
          number1
          title1
          content1
          number2
          title2
          content2
        }
        startsmall{
          heading
          number1
          title1
          content1
          number2
          title2
          content2
        }
        growsmart{
          heading
          number1
          title1
          content1
          number2
          title2
          content2
        }
      }
    }
  }
`
