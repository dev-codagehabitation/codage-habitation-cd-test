import React, { Fragment, useEffect } from 'react'
import { graphql } from 'gatsby'
import { navigate } from '@reach/router'
import { ChevronLeft } from 'react-feather'
import Content from '../../components/Content'
import Layout from '../../components/Layout'
import SinglePostHeader from '../../components/SinglePostHeader'
import './singlepost.scss'
import TagPreview from "../../components/TagWidget/TagPreview"
import { Strings } from '../../resources'
import { checkForRedirection } from "../../utils/page";


export const SinglePostTemplate = ({
  title,
  date,
  body,
  nextPostURL,
  prevPostURL,
  categories = [],
  tags,
  status,
}) => {


  useEffect(() => {
    if (status === "Draft") {
      checkForRedirection(navigate)
    }
  }, []);

  return (

    <main>
      <SinglePostHeader
        darkMode
        title={title}
        date={date}
      />
      <article
        className="singlepost section light"
        itemScope
        itemType="http://schema.org/BlogPosting"
      >
        <div className="container skinny">
          {/* <div onClick={() => navigate(-1)} className="singlepost--backbutton">
           <ChevronLeft /> {Strings.back.toUpperCase()}
        </div> */}
          <TagPreview value={tags} />
          <div className="singlepost--content relative">
            <div className="singlepost--meta">
              {date && (
                <time
                  className="singlepost--meta--date"
                  itemProp="dateCreated pubdate datePublished"
                  date={date}
                >
                  {date}
                </time>
              )}
              {categories && (
                <Fragment>
                  <span>|</span>
                  {categories.map((cat, index) => (
                    <span
                      key={cat.category}
                      className="singlepost--meta--category"
                    >
                      {cat.category}
                      {/* Add a comma on all but last category */}
                      {index !== categories.length - 1 ? ',' : ''}
                    </span>
                  ))}
                </Fragment>
              )}
            </div>

            {title && (
              <h1 className="singlepost--title" itemProp="title">
                {title}
              </h1>
            )}

            <div className="singlepost--innercontent">
              <Content source={body} />
            </div>

          </div>
        </div>
      </article>
    </main>
  )
}

// Export Default SinglePost for front-end
const SinglePost = ({ data: { post, allPosts } }) => {
  const thisEdge = allPosts.edges.find(edge => edge.node.id === post.id)
  return (
    <Layout
      isNavStyle
      meta={post.frontmatter.meta || false}
      title={post.frontmatter.title || false}
    >
      <SinglePostTemplate
        {...post}
        {...post.frontmatter}
        body={post.html}
      />
    </Layout>
  )
}

export default SinglePost

export const pageQuery = graphql`
  ## Query for SinglePost data
  ## Use GraphiQL interface (http://localhost:8000/___graphql)
  ## $id is processed via gatsby-node.js
  ## query name must be unique to this file
  query SinglePost($id: String!) {
    post: markdownRemark(id: { eq: $id }) {
      ...Meta
      html
      id
      frontmatter {
        title
        template
        subtitle
        date(formatString: "MMMM Do, YYYY")
        categories {
          category
        }
        tags
        status
      }
    }

    allPosts: allMarkdownRemark(
      filter: { fields: { contentType: { eq: "posts" } } }
      sort: { order: DESC, fields: [frontmatter___date] }
    ) {
      edges {
        node {
          id
        }
      }
    }
  }
`
