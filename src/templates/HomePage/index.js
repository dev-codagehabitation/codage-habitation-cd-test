import React from 'react'
import { graphql } from 'gatsby'
import { Strings } from "../../resources"
import Layout from '../../components/Layout'
import { Link } from 'gatsby'
import Blogs from '../../components/Blogs'
import Work from '../../components/Work'
import Testimonials from '../../components/Testimonials'
import BookConsultation from '../../components/BookConsultation'
import PageHeader from '../../components/PageHeader'
import Content from '../../components/Content'
import Services from '../../components/Services'

import './home.scss'

const blogsArray = [1, 2, 3]
export const HomePageTemplate = ({ homecontent, homebutton, fullbannerimage, servicescontainer, testimonialscontainer, workcontainer }) => {

  return (
    <main className="home">
      <PageHeader
        bannercontent={homecontent}
        homebutton={homebutton}
        homeBannerImage={fullbannerimage}
      />

      <section className="section services-section dark">
        <div className="container">
          <div className="top-content taCenter">
            <Content source={servicescontainer.headercontent} />
          </div>
          <Services
            items={servicescontainer.servicescontent}
          />
        </div>
      </section>
      {/* <Blogs items={blogsArray} /> */}
      <Work items={workcontainer.workcontent} />
      <BookConsultation />
      <Testimonials items={testimonialscontainer} />
    </main>
  )
}

const HomePage = ({ data: { page } }) => (
  <Layout meta={page.frontmatter.meta || false}>
    <HomePageTemplate {...page} {...page.frontmatter} body={page.html} />
  </Layout>
)

export default HomePage

export const pageQuery = graphql`
  ## Query for HomePage data
  ## Use GraphiQL interface (http://localhost:8000/___graphql)
  ## $id is processed via gatsby-node.js
  ## query name must be unique to this file
  query HomePage($id: String!) {
    page: markdownRemark(id: { eq: $id }) {
      ...Meta
      html
      frontmatter {
        homecontent
        homebutton{
          label
          url
        }
        fullbannerimage{
          image {
            childImageSharp {
              fluid(maxWidth: 800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }   
          alt
        }
        servicescontainer{
          headercontent
          servicescontent{
            title
            description
            serviceslist{
              listitem
            }
            servicesbutton{
              url
            }
          }
        }
        workcontainer{
          headercontent
          workcontent{
            title
            description   
            workimage{
              image {
                childImageSharp {
                  fluid(maxWidth: 800, quality: 100) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }   
              alt
            }        
            worklink{
              url
            }
          }
        }
        testimonialscontainer{
          testimonialscontent
          testimonials{
            authorcontent
            authorname
            authorposition
            authorimage{
              image {
                childImageSharp {
                  fluid(maxWidth: 800, quality: 100) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }   
              alt
            }
          }
        }
      }
    }
  }
`
