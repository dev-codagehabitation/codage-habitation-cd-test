import React from 'react'
import { Strings } from "../../resources"
import { StaticImage } from "gatsby-plugin-image"
import Layout from '../../components/Layout'
import Image from '../../components/Image';
import Button from '../../components/Button'
import Content from '../../components/Content'
import PageHeader from '../../components/PageHeader'
import OurServices from '../../components/OurServices'
import BookConsultation from '../../components/BookConsultation'
import { graphql } from 'gatsby'
import './services.scss'

export const ServicesPageTemplate = ({ servicescontent, ourservicesprovide, WebApplicationDevelopment, mobileApplicationDevelopment, developersQuotes, digitalmarketing, developersQuotes2 }) => {
  return (
    <main className="ServicesPage">
      <PageHeader
        darkMode
        bannercontent={servicescontent}
        className="innerpage--middleContent taCenter"
      />
      <OurServices items={ourservicesprovide} />
      <div className="section dark process-section">
        <div className="container">
          <div className="process-container">
            <StaticImage
              src="../../img/process-shape.svg"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Process Image"
              className="process-shape"
            />
            <div className="process">
              <div className="process-item">
                <span>01</span>
                <h5>Develop</h5>
              </div>
              <div className="process-item">
                <span>02</span>
                <h5>Analysis</h5>
              </div>
              <div className="process-item">
                <span>03</span>
                <h5>Communication</h5>
              </div>
              <div className="process-item">
                <span>04</span>
                <h5>Review</h5>
              </div>
              <div className="process-item">
                <span>05</span>
                <h5>Track</h5>
              </div>
              <div className="process-item">
                <span>06</span>
                <h5>Result</h5>
              </div>
            </div>
            <div className="process-title">Process</div>
          </div>
        </div>
      </div>
      <div className="section services-rowSection">
        <div className="container">
          <div className="services-row services-row-withImage">
            <div className="services-content">
              <Content source={developersQuotes2.content} />
            </div>
            <div className="services-image">
              <Image imageInfo={developersQuotes2.developersquotesimage} />
            </div>
          </div>
        </div>
      </div>
      <BookConsultation />
    </main>
  )
}

const ServicesPage = ({ data: { page } }) => (
  <Layout isNavStyle meta={page.frontmatter.meta || false}>
    <ServicesPageTemplate {...page} {...page.frontmatter} body={page.html} />
  </Layout>
)
export default ServicesPage

export const pageQuery = graphql`
  ## Query for ServicesPage data
  ## Use GraphiQL interface (http://localhost:8000/___graphql)
  ## $id is processed via gatsby-node.js
  ## query name must be unique to this file
  query ServicesPage($id: String!) {
    page: markdownRemark(id: { eq: $id }) {
      ...Meta
      html
      frontmatter {
        servicescontent
        ourservicesprovide{
          content
          knowmorebutton
          applink{
            label
            url
          }
        }
        WebApplicationDevelopment{
          content
          knowmorebutton
          webappbutton{
            label
            url
          }
        }
        mobileApplicationDevelopment{
          content
          knowmorebutton
          webappbutton{
            label
            url
          }
        }
        developersQuotes{
          developersquotesimage{
            image {
              childImageSharp {
                fluid(maxWidth: 800, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }   
            alt
          }
          content
        }
        digitalmarketing{
          content
          knowmorebutton
          webappbutton{
            label
            url
          }
        }
        developersQuotes{
          developersquotesimage{
            image {
              childImageSharp {
                fluid(maxWidth: 800, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }   
            alt
          }
          content
        }
        developersQuotes2{
          developersquotesimage{
            image {
              childImageSharp {
                fluid(maxWidth: 800, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }   
            alt
          }
          content
        }
      }
    }
  }
`
