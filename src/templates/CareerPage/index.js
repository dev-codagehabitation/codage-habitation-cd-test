import React from 'react'
import { Strings } from "../../resources"
import Layout from '../../components/Layout'
import Button from '../../components/Button'
import { StaticImage } from "gatsby-plugin-image"
import Testimonials from '../../components/Testimonials'
import BookConsultation from '../../components/BookConsultation'

import './career.scss'

const testimonialsArray = [
  {
    tesimonialname: 'Matthijs Piëst',
    tesimonialdesignation: 'COO at WieBetaaltWat',
    tesimonialcontent: '“There’s a huge amount of trust we have in their ability to deliver good quality and reliable code. When talking about value, they are in a league of their own.”'
  },
  {
    tesimonialname: 'Matthijs Piëst',
    tesimonialdesignation: 'COO at WieBetaaltWat',
    tesimonialcontent: '“There’s a huge amount of trust we have in their ability to deliver good quality and reliable code. When talking about value, they are in a league of their own.”'
  },
  {
    tesimonialname: 'Matthijs Piëst',
    tesimonialdesignation: 'COO at WieBetaaltWat',
    tesimonialcontent: '“There’s a huge amount of trust we have in their ability to deliver good quality and reliable code. When talking about value, they are in a league of their own.”'
  }
]
export const CareerPageTemplate = ({ }) => {
  return (
    <main className="CareerPage">
      <div className="pageheader pageHeader-container">
        <div className="container">
          <div className="header-content">
            <h2>A strong team anda unique workingenvironment</h2>
            <p>Because only Excellent Team can bring Excellence to your business.</p>
            <Button
              whiteStyle
              label={Strings.PLAN_CREATIVE_BRAINSTORM}
              icon
            />
          </div>
          <div className="banner-image">
            <StaticImage
              src="../../img/career-banner.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Carear Banner"
            />
          </div>
        </div>
        <div className="teamAnimation-container">
          <StaticImage
            src="../../img/team-member.webp"
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Team Member"
          />
        </div>
      </div>
      <div className="companyActivity-carousel">
        <div className="companyActivity-Item">
          <StaticImage
            src="../../img/companyActivity-image1.webp"
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Company Activity"
          />
          <h6>Jinal Is clearing doubts </h6>
        </div>
        <div className="companyActivity-Item">
          <StaticImage
            src="../../img/companyActivity-image2.webp"
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Company Activity"
          />
          <h6>Jinal Is clearing doubts </h6>
        </div>
        <div className="companyActivity-Item">
          <StaticImage
            src="../../img/companyActivity-image3.webp"
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Company Activity"
          />
          <h6>Jinal Is clearing doubts </h6>
        </div>
        <div className="companyActivity-Item">
          <StaticImage
            src="../../img/companyActivity-image4.webp"
            quality={95}
            formats={["auto", "webp", "avif"]}
            alt="Company Activity"
          />
          <h6>Jinal Is clearing doubts </h6>
        </div>
      </div>
      <div className="section wwc-section">
        <div className="container">
          <div className="top-content taCenter">
            <h6 className="opacity">What we create</h6>
            <h3>An agency whereknowledge andproduction gohand-in-hand</h3>
          </div>
          <div className="wwc-container">
            <div className="wwc-item">
              <h2>The <br />Projects</h2>
              <p>Brand platform, digital strategy, corporate identity, naming</p>
            </div>
            <div className="wwc-item">
              <h2>The <br />Projects</h2>
              <p>Brand platform, digital strategy, corporate identity, naming</p>
            </div>
            <div className="wwc-item">
              <h2>The <br />Projects</h2>
              <p>Brand platform, digital strategy, corporate identity, naming</p>
            </div>
            <div className="wwc-item">
              <h2>The <br />Projects</h2>
              <p>Brand platform, digital strategy, corporate identity, naming</p>
            </div>
          </div>
        </div>
      </div>
      <div className="activityDays-section">
        <div className="container">
          <div className="top-content taCenter">
            <h6 className="opacity">Activity days</h6>
            <h3>Our company activity days might include sport, parties, adventure or training.</h3>
          </div>
          <div className="activityDays-container">
            <StaticImage
              src="../../img/activity-days.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Activity Days"
            />
          </div>
        </div>
      </div>
      <Testimonials items={testimonialsArray} />
      <div className="codage-habitation">
        <div className="container">
          <div className="codage-habitation-container">
            <StaticImage
              src="../../img/showcase-image1.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Codage Habitation"
            />
            <StaticImage
              src="../../img/showcase-image2.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Codage Habitation"
            />
            <StaticImage
              src="../../img/showcase-image3.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Codage Habitation"
            />
            <StaticImage
              src="../../img/showcase-image4.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Codage Habitation"
            />
            <StaticImage
              src="../../img/showcase-image5.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Codage Habitation"
            />
            <StaticImage
              src="../../img/showcase-image6.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Codage Habitation"
            />
          </div>
          <div className="codage-habitation-textStyle">#CODAGEHABITATION</div>
        </div>
      </div>
      <BookConsultation />
    </main>
  )
}

const CareerPage = ({ }) => (
  <Layout isNavStyle>
    <CareerPageTemplate />
  </Layout>
)

export default CareerPage