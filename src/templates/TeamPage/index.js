import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/Layout'
import Image from '../../components/Image';
import Content from '../../components/Content'
import PageHeader from '../../components/PageHeader'

import './team.scss'

export const TeamPageTemplate = ({ teamcontent, teamBannerImage, bannerButtons, teamtitle, meetTheTeam }) => {
  return (
    <main className="TeamPage">
      <PageHeader
        darkMode
        bannercontent={teamcontent}
        bannerButtons={bannerButtons}
        className="innerpage--content team--headercontent"
      />
      <div className="banner-container">
        <div className="container">
          <div className="banner-image">
            <Image imageInfo={teamBannerImage} />
          </div>
        </div>
      </div>
      <div className="section meetTeam-section">
        <div className="container">
          {teamtitle && (
            <h2>{teamtitle}</h2>
          )}
          {!!meetTheTeam &&
            meetTheTeam.map((item, index) => (
              <div className="teamInfo-row" key={index}>
                <div className="team-info">
                  <h6>{item.name}</h6>
                  <span>{item.position}</span>
                  <Content source={item.content} className="infoText" />
                  <p>{item.notes}</p>
                </div>
                <div className="team-photo">
                  <Image imageInfo={item.teamMemberImage} />
                </div>
              </div>
            )
            )
          }
        </div>
      </div>
    </main >
  )
}


const TeamPage = ({ data: { page } }) => (
  <Layout isNavStyle meta={page.frontmatter.meta || false}>
    <TeamPageTemplate {...page} {...page.frontmatter} body={page.html} />
  </Layout>
)

export default TeamPage

export const pageQuery = graphql`
  ## Query for TeamPage data
  ## Use GraphiQL interface (http://localhost:8000/___graphql)
  ## $id is processed via gatsby-node.js
  ## query name must be unique to this file
  query TeamPage($id: String!) {
    page: markdownRemark(id: { eq: $id }) {
      ...Meta
      html
      frontmatter {
        teamcontent
        bannerButtons{
          label
          url
        }
        teamBannerImage{
          image {
            childImageSharp {
              fluid(maxWidth: 800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }   
          alt
        }
        teamtitle
        meetTheTeam{
          name
          position
          content
          notes
          teamMemberImage{
            image {
              childImageSharp {
                fluid(maxWidth: 800, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }   
            alt
          }
        }
      }
    }
  }
`
