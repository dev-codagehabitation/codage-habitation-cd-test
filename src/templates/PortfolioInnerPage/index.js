import React from 'react'
import { Strings } from "../../resources"
import Layout from '../../components/Layout'
import Button from '../../components/Button'
import PageHeader from '../../components/PageHeader'
import Stories from '../../components/Stories'
import { StaticImage } from "gatsby-plugin-image"
import workStrings from '../../data/work'

import './portfolioInner.scss'

const blogsArray = [1, 2, 3]
export const PortfolioInnerPageTemplate = ({ title }) => {
  return (
    <main className="PortfolioInnerPage">
      <div className='pageheader relative innerpage--content team--headercontent pageheader-dark'>
        <div className="container">
          <div className="header-content">
            <h2 className="font-normal">What we make,Makes us.</h2>
            <p>Because only Excellent Team can bring Excellence to your business.</p>
            <div className='buttons-group'>
              <div className='ButtonItem'>
                <Button
                  whiteStyle
                  label={Strings.WATCH_WEBSITE_LIVE}
                  icon
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="banner-container">
        <div className="container">
          <div className="banner-image">
            <StaticImage
              src="../../img/portfolio-image.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Grow and Escalate"
            />
          </div>
        </div>
      </div>
      <div className="section showcase">
        <div className="container">
          <div className="showcase-container">
            <div className="showcase-main">
              <StaticImage
                src="../../img/showcase-image1.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
            </div>
            <div className="showcase-thubmails">
              <StaticImage
                src="../../img/showcase-image2.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image3.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image4.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image5.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image6.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image7.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image8.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image9.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
              <StaticImage
                src="../../img/showcase-image10.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Grow and Escalate"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="dark online-ordering">
        <div className="container">
          <h2>{workStrings.from_orientation_to_online_ordering}</h2>
          <p className="opacity">{workStrings.from_orientation_to_online_ordering_description}</p>
          <h2>{workStrings.simple_and_fast}</h2>
          <p className="opacity">{workStrings.from_orientation_to_online_ordering_description}</p>
        </div>
      </div>
      <div className="online-ordering-images">
        <div className="container">
          <div className="online-ordering-container">
            <StaticImage
              src="../../img/showcase-image12.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Grow and Escalate"
            />
            <StaticImage
              src="../../img/showcase-image12.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Grow and Escalate"
            />
            <StaticImage
              src="../../img/showcase-image13.webp"
              quality={95}
              formats={["auto", "webp", "avif"]}
              alt="Grow and Escalate"
            />
          </div>
          <div className="optimizing-footer">
            <h2 className="font-normal">{workStrings.continuous_testing_and_optimizing}</h2>
            <div className="social">
              <a className="facebook"></a>
              <a className="insta"></a>
              <a className="twitter"></a>
              <a className="linkdin"></a>
            </div>
            <p>{workStrings.share_text}</p>
          </div>
        </div>
      </div>
      <div className="section success-stories">
        <div className="container">
          <h2>{workStrings.read_stories}</h2>
          <Stories items={blogsArray} />
        </div>
      </div>
    </main>
  )
}

const PortfolioInnerPage = ({ }) => (
  <Layout isNavStyle>
    <PortfolioInnerPageTemplate />
  </Layout>
)

export default PortfolioInnerPage