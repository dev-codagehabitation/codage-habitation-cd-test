import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/Layout'
import { StaticImage } from "gatsby-plugin-image"
import Portfolio from '../../components/Portfolio'
import FeaturedCases from '../../components/FeaturedCases'

import './portfolio.scss'
import { Strings } from '../../resources';
import PageHeader from '../../components/PageHeader';
import workStrings from '../../data/work'

// const blogsArray = [1, 2, 3]
const featuredCases = [1, 2, 3]
const OurCasessArray = [1, 2, 3, 4]
const LetsWorkArray = [1, 2, 3, 4, 5, 6]
export const PortfolioPageTemplate = ({ title, portfoliocontent }) => {
  return (
    <main className="PortfolioPage pageheader innerpage--content">
      <PageHeader
        darkMode
        bannercontent={portfoliocontent}
        className="innerpage--content portfolio--headercontent"
      />
      <FeaturedCases
        title={workStrings.featuredCases}
      />

      <Portfolio
        title={workStrings.ourcases}
        items={OurCasessArray}
        className="section portfolio-workSection"
      />
      <Portfolio
        title={workStrings.letsWork}
        items={LetsWorkArray}
        className="portfolio-workSection"
        ButtonLabel={workStrings.startIntake}
        loadMore
      />
    </main>
  )
}


const PortfolioPage = ({ data: { page } }) => (
  <Layout isNavStyle meta={page.frontmatter.meta || false}>
    <PortfolioPageTemplate {...page} {...page.frontmatter} body={page.html} />
  </Layout>
)

export default PortfolioPage

export const pageQuery = graphql`
  ## Query for PortfolioPage data
  ## Use GraphiQL interface (http://localhost:8000/___graphql)
  ## $id is processed via gatsby-node.js
  ## query name must be unique to this file
  query PortfolioPage($id: String!) {
    page: markdownRemark(id: { eq: $id }) {
      ...Meta
      html
      frontmatter {
        portfoliocontent
      }
    }
  }
`
