import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/Layout'
import Image from '../../components/Image';
import Content from '../../components/Content'
import PageHeader from '../../components/PageHeader'
import BookConsultation from '../../components/BookConsultation'
import Services from '../../components/Services'

import './company.scss'

export const CompanyPageTemplate = ({ companycontent, companyBannerImage, codagecontainer, companyservicescontainer, globaldigitaltrends, companyworkcontainer }) => {
  return (
    <main className="company">
      <PageHeader
        darkMode
        bannercontent={companycontent}
        className="innerpage--content"
      />
      <div className="banner-container">
        <div className="container">
          <div className="banner-image">
            <Image imageInfo={companyBannerImage} />
          </div>
        </div>
      </div>
      <div className="section codage">
        <div className="codage_container">
          <div className="left">
            {codagecontainer.title && (
              <h2>{codagecontainer.title}</h2>
            )
            }
          </div>
          <div className="right">
            <Content source={codagecontainer.content} />
          </div>
        </div>
      </div>
      <div className="services-section">
        <div className="container">
          <div className="top-content">
            <Content source={companyservicescontainer.headercontent} />
          </div>
          <Services items={companyservicescontainer.servicescontent} />
        </div>
      </div>
      <div className="reviews-section">
        <div className="container">
          <div className="dark-container dark">
            <div className="dark-content">
              <Content source={globaldigitaltrends.content} />
            </div>
          </div>
        </div>
      </div>
      <div className="section work-companies-section">
        <div className="container">
          <div className="work-companies-container">
            <div className="left">
              <Image imageInfo={companyworkcontainer.leftimage} />
            </div>
            <div className="right">
              <Image imageInfo={companyworkcontainer.rightimage} />
            </div>
          </div>
          <div className="work-companies-content">
            <Content source={companyworkcontainer.content} />
          </div>
        </div>
      </div>
      <BookConsultation />
    </main >
  )
}


const CompanyPage = ({ data: { page } }) => (
  <Layout isNavStyle meta={page.frontmatter.meta || false}>
    <CompanyPageTemplate {...page} {...page.frontmatter} body={page.html} />
  </Layout>
)

export default CompanyPage

export const pageQuery = graphql`
  ## Query for CompanyPage data
  ## Use GraphiQL interface (http://localhost:8000/___graphql)
  ## $id is processed via gatsby-node.js
  ## query name must be unique to this file
  query CompanyPage($id: String!) {
    page: markdownRemark(id: { eq: $id }) {
      ...Meta
      html
      frontmatter {
        companycontent
        companyBannerImage{
          image {
            childImageSharp {
              fluid(maxWidth: 800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }   
          alt
        }
        codagecontainer{
          title
          content
        }
        companyservicescontainer{
          headercontent
          servicescontent{
            title
            description
          }
        }
        globaldigitaltrends{
          content
        }
        companyworkcontainer{
          leftimage{
            image {
              childImageSharp {
                fluid(maxWidth: 800, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }   
            alt
          }
          rightimage{
            image {
              childImageSharp {
                fluid(maxWidth: 800, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }   
            alt
          }
          content
        }
      }
    }
  }
`
