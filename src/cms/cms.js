import React from 'react'
import CMS from 'netlify-cms-app'
import { TagControl, TagPreview } from '../components/TagWidget';


import { HomePageTemplate } from '../templates/HomePage'
import { CompanyPageTemplate } from '../templates/CompanyPage'
import { ServicesPageTemplate } from '../templates/ServicesPage'
import { TeamPageTemplate } from '../templates/TeamPage'
import { ApprochPageTemplate } from '../templates/ApprochPage'

import uploadcare from 'netlify-cms-media-library-uploadcare'
import styles from "./previewcms.scss"

// import HomePagePreview from './preview-templates/HomePagePreview'


CMS.registerMediaLibrary(uploadcare)
CMS.registerWidget('tags', TagControl, TagPreview)


// CMS.registerPreviewTemplate('home-page', HomePagePreview)

CMS.registerPreviewTemplate('home-page', ({ entry }) => (
  <HomePageTemplate {...entry.toJS().data} />
))
CMS.registerPreviewTemplate('company-page', ({ entry }) => (
  <CompanyPageTemplate {...entry.toJS().data} />
))
CMS.registerPreviewTemplate('services-page', ({ entry }) => (
  <ServicesPageTemplate {...entry.toJS().data} />
))
CMS.registerPreviewTemplate('team-page', ({ entry }) => (
  <TeamPageTemplate {...entry.toJS().data} />
))
CMS.registerPreviewTemplate('approch-page', ({ entry }) => (
  <ApprochPageTemplate {...entry.toJS().data} />
))
