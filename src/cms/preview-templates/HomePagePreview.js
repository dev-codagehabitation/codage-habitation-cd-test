import React from 'react'
import PropTypes from 'prop-types'
import { HomePageTemplate } from '../../templates/HomePage'

const HomePagePreview = ({ entry, getAsset }) => {
  const data = entry.getIn(['data']).toJS()

  if (data) {
    return (
      <HomePageTemplate 
        title={entry.getIn(['data', 'title'])}
        subtitle={entry.getIn(['data', 'subtitle'])}
        body={entry.getIn(['data', 'body'])}
        featuredimage={{
          image: entry.getIn(['data','featuredimage', 'image']),          
        }}


      />
    )
  } else {
    return <div>Loading...</div>
  }
}

HomePagePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  getAsset: PropTypes.func,
}

export default HomePagePreview