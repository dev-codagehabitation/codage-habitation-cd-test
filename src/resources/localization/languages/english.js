
export default {
  LETS_DISCUSS: 'Lets discuss',
  TO_COLLABRATE: 'to collabrate',
  VACANIES: 'Vacanies',
  BOOK_FREE_CONSULTATION: 'Book free consultation',
  KNOW_MORE: 'Know More',
  LOADMORE: 'Load More',
  WATCH_WEBSITE_LIVE: 'Watch Website live',
  PLAN_CREATIVE_BRAINSTORM: 'Plan creative brainstorm',
  PEOPLE: 'People',
  CAREERS: 'Careers',
  Explore_Services: "Explore Services",
  blogs: "Blogs",
  what_clients_says: "What clients says about us",
  see_all: "See all",
  close_menu: "Close menu",
  all_category_text: "All Posts",
  email_form_text: "Supercharge your brain",
  lets_learn: "Let’s learn",
  recent_articles_blog: "Recent articles from our web development blog",
  blog_search_text:"Search"
}
