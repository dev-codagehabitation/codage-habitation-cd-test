import React, { useState, useRef } from "react";
import Layout from "../../components/Layout";
import { Helmet } from "react-helmet";
import { Strings } from "../../resources";
import { StaticImage } from "gatsby-plugin-image";
import "./contact.scss";

export default function ContactUs() {
  const [submitted, setSubmitted] = useState(false);
  const company_input_ref = useRef();
  const name_input_ref = useRef();
  const email_input_ref = useRef();
  const phone_input_ref = useRef();
  const message_input_ref = useRef();
  const checkbox_input_ref = useRef();

  const validateForm = () => {
    var error = [];

    if (name_input_ref.current.value === "") {
      error.push("name");
    }
    if (!email_input_ref.current.value.includes("@")) {
      error.push("email");
    }
    if (checkbox_input_ref.current.checked === false) {
      error.push("checkbox");
    }
    return error;
  };
  const submitHandler = (e) => {
    e.preventDefault();

    var err = validateForm();
    for (var key in err) {
      switch (err[key]) {
        case "name":
          name_input_ref.current.className = "form-control error";
          document.getElementById("firstName").focus();
          break;
        case "email":
          email_input_ref.current.className = "form-control error";
          document.getElementById("email").focus();
          break;
        case "checkbox":
          checkbox_input_ref.current.className = "form-control error";
          document.getElementById("terms").focus();
          break;
        default:
          break;
      }
    }
    var data = {
      company: company_input_ref.current.value,
      name: name_input_ref.current.value,
      email: email_input_ref.current.value,
      phone: phone_input_ref.current.value,
      message: message_input_ref.current.value,
    };
    if (err.length === 0) {
      fetch("http://codage-habitation-backend.devserapp.com/entries/add", {
        method: "POST",
        mode:'cors',
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "Application/json",
          
        },                      
      })
        .then((res) => res.json())
        .then((res) => {
          console.log(res);
          setSubmitted(true);
        })
        .catch((err) => console.log(err));
    }
  };
  const contact_us = (
    <div className="contact-info">
      <h1>hello.</h1>
      <div className="contact-row">
        <h5>
          <a href="mailto:info@codagehabitation.com">
            info@codagehabitation.com
          </a>
        </h5>
        <h6>
          <a href="tel:+919714148940">+91 971 414 8940</a>
        </h6>
      </div>
      <div className="contact-row">
        <h6 className="title">Office</h6>
        <p>Fortune Business Hub, Science City Rd, Ahmedabad, Gujarat 380060</p>
        <p>
          <a
            href="https://goo.gl/maps/m9VuH6Uyy4RwZPJA9"
            target="_blank"
            className="link-directions"
          >
            Directions
          </a>
        </p>
      </div>
      <div className="contact-row">
        <h6 className="title">
          We are open for great idea (almost) every day!
        </h6>
        <div className="timeRow">
          <div className="time-label">
            <p>Mon - Fri</p>
          </div>
          <div className="time-value">
            <p>9:30 - 19:00</p>
          </div>
        </div>
        <div className="timeRow">
          <div className="time-label">
            <p>Sat to Sun</p>
          </div>
          <div className="time-value">
            <p>Offline</p>
          </div>
        </div>
      </div>
    </div>
  );

  const contact_form = (
    <div className="contactForm-container">
      <h2>Send us message!</h2>
      <h4 className="font-normal">Good plan.its just might change your life</h4>
      {!submitted && (
        <form
          name="Contact Form"
          onSubmit={submitHandler}
          className="contactForm"
        >
          <div className={`frm`}>
            <input
              type="text"
              className="form-control"
              id="companyName"
              placeholder="Company Name"
              ref={company_input_ref}
            />
          </div>
          <div className={`frm`}>
            <input
              type="text"
              className="form-control"
              id="firstName"
              placeholder="First and last name*"
              ref={name_input_ref}
            />
          </div>
          <div className={`frm`}>
            <input
              type="text"
              className="form-control email"
              id="email"
              placeholder="E-mail address*"
              ref={email_input_ref}
            />
          </div>
          <div className={`frm`}>
            <input
              type="tel"
              maxLength="10"
              className="form-control"
              id="phone"
              name="phone"
              placeholder="Phone number"
              ref={phone_input_ref}
            />
          </div>
          <div className={`frm`}>
            <input
              type="text"
              className="form-control"
              id="message"
              name="message"
              placeholder="Enter your message here"
              ref={message_input_ref}
            />
          </div>

          <div className="checkbox">
            <div className="auth-check-wrap">
              <input
                type="checkbox"
                className="form-control"
                id="terms"
                name="messtermsage"
                placeholder="Enter your message here*"
                ref={checkbox_input_ref}
              />
              <span class="auth-checkbox"></span>
            </div>
            <div className="checkbox-lable">
              I agree with the <a href="#">Terms and Conditions</a>
            </div>
          </div>

          <div className="form-buttom">
            <button type="submit" className={`button btnIcon blackStyle`}>
              Book free consultation
            </button>
            <p className="small-text">
              Of course we handle your data well.
              <br />
              With this request you immediately agree with us{" "}
              <a href="#">privacy statement</a>
            </p>
          </div>
        </form>
      )}
      {submitted && <h1 className="thankyou">Thank You!</h1>}
    </div>
  );

  return (
    <Layout>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{Strings.contacttitle}</title>
        <meta name="description" content={Strings.contactdescription} />
      </Helmet>
      <main className="contact">
        <div className="container">{contact_us}</div>
        <div className="container">{contact_form}</div>
        {/* <div className="tackle_brand-section">
          <div className="tackle_brand-images">
            <div>
              <StaticImage
                src="../../img/office-image1.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Codage Habitation"
              />
            </div>
            <div>
              <StaticImage
                src="../../img/office-image2.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Codage Habitation"
              />
              <StaticImage
                src="../../img/office-image3.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Codage Habitation"
              />
            </div>
            <div>
              <StaticImage
                src="../../img/office-image4.webp"
                quality={95}
                formats={["auto", "webp", "avif"]}
                alt="Codage Habitation"
              />
            </div>
          </div>
        </div> */}
        
      </main>
    </Layout>
  );
}
