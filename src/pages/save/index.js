import React from 'react'
import Layout from '../../components/Layout'
import { Helmet } from "react-helmet"
import { Strings } from "../../resources"
import Dropdown from 'react-dropdown';
import './save.scss'


const index = (props) => {

  const options = [
    'Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'
  ];

  return (
    <Layout>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{Strings.contacttitle}</title>
        <meta name="description" content={Strings.contactdescription} />
      </Helmet>
      <main className="contact">
        <div className="breadcrumb-container">
          <div className="container">
            <ul className="breadcrumb">
              <li><a href="/">{Strings.home}</a></li>
              <li>{Strings.save}</li>
            </ul>
          </div>
        </div>
        <div className="save_container">
          <div className="container">
            <div className="content">
              <h2>{Strings.save_title}</h2>
              <p><span>{Strings.save_desc}</span></p>
            </div>
            <div className="save-row">
              <div className="contact-content">
                <div className="contact-info">
                  <div className="contact-item">
                    <span>1</span>
                    <p>{Strings.save_row_text_1}</p>
                  </div>
                  <div className="contact-item">
                    <span>2</span>
                    <p>{Strings.save_row_text_2}</p>
                  </div>
                  <div className="contact-item">
                    <span>3</span>
                    <p>{Strings.save_row_text_3}</p>
                  </div>
                </div>
                <p>{Strings.save_notes}</p>
              </div>
              <div className="contact-form">
                <form name="contact" method="post" data-netlify="true" data-netlify-honeypot="bot-field" className="contact-frm">
                  <div className="frm">
                    <label htmlFor="discountcode">{Strings.discount_code}</label>
                    <input type="text" name="discountcode" id="discountcode" />
                  </div>
                  <div className="frm">
                    <label htmlFor="email">{Strings.email}</label>
                    <input type="email" name="email" id="email" required />
                  </div>
                  <div className="frm">
                    <label htmlFor="doctor">{Strings.doctor_who_recommended_CANNACEUTICA}</label>
                    <input type="text" name="doctor" id="doctor" />
                  </div>
                  <div className="frm">
                    <label htmlFor="state">{Strings.state}</label>
                               
                      <Dropdown
                        options={options}
                        // onChange={event => onSelectSubject(event)}
                        // value={selectedSubject}
                        placeholder={"Select an option"}
                        id="subject"
                        controlClassName="select-control"
                      />   
                    
                  </div>
                  <button type="submit" className={`button`}>{Strings.submit}</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
    </Layout>
  )

}
export default index;