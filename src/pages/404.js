import React from 'react'
import Helmet from 'react-helmet'
import { Link, StaticQuery, graphql } from 'gatsby'
import { Strings } from '../resources/localization'

import Layout from '../components/Layout'

export default ({ children }) => (
  <StaticQuery
    query={graphql`
      query NotFoundPageQuery {
        globalSettings: settingsYaml {
          siteTitle
        }
      }
    `}
    render={data => (
      <Layout>
        <Helmet>
          <title>{Strings.notfoundtitle}</title>
        </Helmet>
        <section className="section">
          <div className="container">
            <div className="notfoundcontent-container">
              <div className="notfoundcontent">
                <h1 class="notFound-title">{Strings.notfoundtitle}</h1>
                <h2>
                  {Strings.notfoundcontent}
                </h2>
                <div className="mobile-center">
                  <Link to="/" className="button">Home</Link>
                </div>
              </div>
              <div className="notfoundImg">

              </div>
            </div>
          </div>
        </section>
      </Layout>
    )}
  />
)
