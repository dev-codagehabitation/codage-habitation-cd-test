exports.components = {
  "component---cache-dev-404-page-js": () => import("./../../dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-404-js": () => import("./../../../src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */),
  "component---src-pages-contact-index-js": () => import("./../../../src/pages/contact/index.js" /* webpackChunkName: "component---src-pages-contact-index-js" */),
  "component---src-pages-save-index-js": () => import("./../../../src/pages/save/index.js" /* webpackChunkName: "component---src-pages-save-index-js" */),
  "component---src-templates-approch-page-index-js": () => import("./../../../src/templates/ApprochPage/index.js" /* webpackChunkName: "component---src-templates-approch-page-index-js" */),
  "component---src-templates-blog-index-index-js": () => import("./../../../src/templates/BlogIndex/index.js" /* webpackChunkName: "component---src-templates-blog-index-index-js" */),
  "component---src-templates-career-page-index-js": () => import("./../../../src/templates/CareerPage/index.js" /* webpackChunkName: "component---src-templates-career-page-index-js" */),
  "component---src-templates-company-page-index-js": () => import("./../../../src/templates/CompanyPage/index.js" /* webpackChunkName: "component---src-templates-company-page-index-js" */),
  "component---src-templates-home-page-index-js": () => import("./../../../src/templates/HomePage/index.js" /* webpackChunkName: "component---src-templates-home-page-index-js" */),
  "component---src-templates-portfolio-inner-page-index-js": () => import("./../../../src/templates/PortfolioInnerPage/index.js" /* webpackChunkName: "component---src-templates-portfolio-inner-page-index-js" */),
  "component---src-templates-portfolio-page-index-js": () => import("./../../../src/templates/PortfolioPage/index.js" /* webpackChunkName: "component---src-templates-portfolio-page-index-js" */),
  "component---src-templates-services-page-index-js": () => import("./../../../src/templates/ServicesPage/index.js" /* webpackChunkName: "component---src-templates-services-page-index-js" */),
  "component---src-templates-single-post-index-js": () => import("./../../../src/templates/SinglePost/index.js" /* webpackChunkName: "component---src-templates-single-post-index-js" */),
  "component---src-templates-team-page-index-js": () => import("./../../../src/templates/TeamPage/index.js" /* webpackChunkName: "component---src-templates-team-page-index-js" */)
}

